
public class FindTotal {

	static int count = MyUserInput.readNumber("Enter a Number");
	static int total = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for (int n = 1; n <= count; n++) {
			System.out.println(n);
			total = total + n;
		}

		System.out.println("----\n" + total + "\n----");

	}

}

//output
/* 
Enter a Number:3
1
2
3
----
6
---- */
